#!/usr/bin/env python3
import configparser
import email.policy
import http.client
import json
import os.path
import sys

from datetime import datetime
from email.parser import BytesParser
from typing import Dict
from urllib.parse import urlparse


class ReturnStatusExit(SystemExit):
    def __init__(self, code: int = 0, msg: str = "Ok", dsn: str = "250") -> None:
        print(f"{dsn} {msg}")
        super().__init__(code)


def main() -> None:
    config = configparser.ConfigParser()
    script_path: str = os.path.abspath(os.path.dirname(__file__))
    cfg_file_name: str = "slackpipe.cfg"

    config.read(
        (
            os.path.join(script_path, cfg_file_name),
            cfg_file_name,
            "/etc/slackpipe/slackpipe.cfg",
            "/etc/slackpipe.cfg",
        )
    )

    try:
        channel: str = config.get("slackpipe", "channel").strip("'\"")
        username: str = config.get("slackpipe", "username").strip("'\"")
        webhook: str = config.get("slackpipe", "webhook").strip("'\"")
    except (configparser.NoSectionError, configparser.NoOptionError) as err:
        print(f"Configuration error: {err}")
        raise SystemExit(1)

    hook_url_parts = urlparse(webhook)

    mailmsg = BytesParser(policy=email.policy.HTTP).parse(sys.stdin.buffer)
    msg_subject: str = mailmsg.get("Subject", "")
    msg_date: str = mailmsg.get("Date", datetime.now().strftime("%c"))
    msg_from: str = mailmsg.get("From", "")
    msg_body: str = mailmsg.get_payload()
    msg = f"Date: {msg_date}\nFrom: {msg_from}\nSubject: {msg_subject}\n\n{msg_body}"

    payload: Dict[str, str] = {
        "text": msg,
        "channel": channel,
        "username": username,
    }

    try:
        conn = http.client.HTTPSConnection(hook_url_parts.netloc)
        data = json.dumps(payload)
        headers = {"Content-Type": "application/json"}
        conn.request("POST", hook_url_parts.path, data, headers)
        resp = conn.getresponse()
        conn.close()
        status: str = ""
    except Exception as err:
        status = "Delivery failed, try again later - {}".format(err)
        raise ReturnStatusExit(code=75, msg=status, dsn="4.3.0")

    if resp.status == 200:
        raise ReturnStatusExit(code=0, msg="Ok: Message sent.")
    else:
        status = "Delivery failed, try again later - {}".format(resp.read().decode())
        raise ReturnStatusExit(code=75, msg=status, dsn="4.5.0")


if __name__ == "__main__":
    main()
