# Postfix Slack Pipe

A postfix pipe filter that allows for the delivery of email directly
to a slack channel via a slack webhook.

## Requirements

This script should only need python 3.6 or newer with no external dependencies.
Therefore, the system python 3 available on Debian 10 or newer or Ubuntu 18.04
or newer should be sufficient.

## Installation

Simply put the `slack_pipe.py` script somewhere on your system (for example in
`/usr/local/bin`), and make it executable.

```
    sudo mv slack_pipe.py /usr/local/bin
    sudo chmod a+rx /usr/local/bin/slack_pipe.py
```

Then set up `slackpipe.cfg` config file. This is a standard ini file using
the format of the python stdlib configparser. There is a sample config
included in the contrib directory of this git repository.

By default, the `slack_pipe.py` script looks for config in either the same
directory as the `slack_pipe.py` script, or at `/etc/slackpipe.cfg` or
`/etc/slackpipe/slackpipe.cfg`. So for example, you can copy the example
script, like so:

```
    sudo mv contrib/slackpipe.cfg /etc
```

Then use your favorite editor to set your webhook url, etc. You will also
need to adjust the ownership/permissions of the config file so that the user
that postfix will execute the script as has read permissions on the config.


Finally, add the script as a transport for postfix by adding it to your
`/etc/postfix/master.cf` (location of the master.cf may be different depending
on your OS). You can add a line like the following to the bottom of your
`master.cf`:

```
slackpipe    unix  -       n       n       -       -       pipe
    flags=BDFRXq user=nobody argv=/usr/local/bin/slack_pipe.py ${nexthop}
```

Adjust the user part to be the user on the system to run the script as
(remember that the user specified will need read permissions to the
configuration file).

You can now add `slackpipe` as a destination/transport in your postfix
configuration. You can even make it your only/default transport so that
ALL emails sent from your system get routed to slack instead (probably
not what you want), or you can use the `transport_maps` setting to create
a map of email addresses/domains to route to the slack channel. This,
however, is a bit out of scope of this readme, so I'll refer you to the
postfix documentation.
